//
//  ProfileViewController.swift
//  TikTok
//
//  Created by Ganesh Bisht on 10/08/20.
//  Copyright © 2020 TikTok. All rights reserved.
//


import UIKit
import SwiftyDropbox
import Photos



class ProfileViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    @IBOutlet weak var uploadDropBoxButton: RoundCornerButton!
    
    // Initialize with manually retrieved auth token
    let client = DropboxClient(accessToken: DROPBOX_ACCESS_TOKEN)
    
    var imageSet =  ["hulk-1","venom","toronto","spiderman","thanos","toronto","venom","hulk-1","war_machine"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        collectionView.reloadData()
    }
    
    
    
    @IBAction func uploadDropBoxTapped(_ sender: Any) {
        
        presentPhotoVideoInputActionsheet()
        
    }
    
    private func presentPhotoVideoInputActionsheet() {
        let actionSheet = UIAlertController(title: "Attach Photo/Video",
                                            message: "What you want to upload",
                                            preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Photo", style: .default, handler: { [weak self] _ in
            
            self?.presentPhotoInputActionsheet()
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Video", style: .default, handler: { [weak self] _ in
            
            self?.presentVideoInputActionsheet()
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        present(actionSheet, animated: true)
        
    }
    
    
    
    
    private func presentPhotoInputActionsheet() {
        let actionSheet = UIAlertController(title: "Attach Photo",
                                            message: "Where would you like to attach a photo from",
                                            preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { [weak self] _ in
            
            let picker = UIImagePickerController()
            picker.sourceType = .camera
            picker.delegate = self
            picker.allowsEditing = true
            self?.present(picker, animated: true)
            
        }))
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { [weak self] _ in
            
            let picker = UIImagePickerController()
            picker.sourceType = .photoLibrary
            picker.delegate = self
            picker.allowsEditing = true
            self?.present(picker, animated: true)
            
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        present(actionSheet, animated: true)
    }
    
    private func presentVideoInputActionsheet() {
        let actionSheet = UIAlertController(title: "Attach Video",
                                            message: "Where would you like to attach a video from?",
                                            preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { [weak self] _ in
            
            let picker = UIImagePickerController()
            picker.sourceType = .camera
            picker.delegate = self
            picker.mediaTypes = ["public.movie"]
            picker.videoQuality = .typeMedium
            picker.allowsEditing = true
            self?.present(picker, animated: true)
            
        }))
        actionSheet.addAction(UIAlertAction(title: "Library", style: .default, handler: { [weak self] _ in
            
            let picker = UIImagePickerController()
            picker.sourceType = .photoLibrary
            picker.delegate = self
            picker.allowsEditing = true
            picker.mediaTypes = ["public.movie"]
            picker.videoQuality = .typeMedium
            self?.present(picker, animated: true)
            
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        present(actionSheet, animated: true)
    }

    
    
}

// UIImagePickerControllerDelegate

extension ProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        
        
        if let image = info[.editedImage] as? UIImage, let imageData =  image.pngData() {
            
            guard let fileUrl = info[UIImagePickerController.InfoKey.imageURL] as? URL else { return }
                   print(fileUrl.lastPathComponent) // get file Name
                   print(fileUrl.pathExtension)     // get file extension
                   
                   
            
            let fileName = "photo_" + fileUrl.lastPathComponent.replacingOccurrences(of: " ", with: "-")
            
            // Upload image
            client.files.upload(path: "/" + fileName, input: imageData)
                .response { response, error in
                    if let response = response {
                        print(response)
                    } else if let error = error {
                        print(error)
                    }
            }
            .progress { progressData in
                print(progressData)
            }
            
            
        }
        else if let videoUrl = info[.mediaURL] as? URL {
            
            print(videoUrl)
            
            let fileName = "video_" + String(Int.random(in: 0..<1000)) + ".mp4"
            
            
            do{
        
            
            // Upload image
            client.files.upload(path: "/" + fileName, input: try Data.init(contentsOf: videoUrl))
                .response { response, error in
                    if let response = response {
                        print(response)
                    } else if let error = error {
                        print(error)
                    }
            }
            .progress { progressData in
                print(progressData)
            }
                
            }catch{
                print(error)
            }
            
        }
    }
}


extension ProfileViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageSet.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "profileItemCell", for: indexPath) as! ProfileItemCollectionViewCell
        cell.itemImageView.image = UIImage(named: imageSet[indexPath.row])
        return cell
    }
    
    
    
}
